#+title: System Crafters Community Guidelines

The following guidelines are designed to foster a friendly, collaborative environment in the System Crafters community.

[NOTE: These guidelines are currently in draft form, contribute your feedback in [[https://forum.systemcrafters.net/t/feedback-needed-system-crafters-community-guidelines][this forum post]].]

* Guidelines

** 1. Be Excellent to Each Other!

Treat all members with respect, kindness, and empathy. Respect diverse backgrounds, opinions and viewpoints. Any form of harassment or discrimination is not tolerated.

We are a diverse community, not only in our choice of software and tools, but also in the identities and backgrounds of the people who are part of it.  We want all newcomers to feel welcome!

** 2. Encourage Learning and Growth

Our community values learning and personal growth. Encourage others in their learning journey. Remember, everyone was a beginner at some point.

Engage in constructive and positive communication. Avoid negative remarks that could harm the community atmosphere. Offer constructive feedback and be open to receiving it.

** 3. Avoid Divisive Topics

Steer clear of topics that are typically divisive and unrelated to our core interests. This includes, but is not limited to, discussions on politics, religion, and other sensitive social issues.

Our focus is on building and learning about custom computer configurations, programming, Free Software, and related technical subjects. While these other topics are important, they often detract from the collaborative nature of our community.

** 4. Respect Personal Privacy and Boundaries

Recognize and respect personal boundaries. Understand that everyone has different levels of comfort in interaction, and adjust your behavior accordingly.

Also respect the privacy and confidentiality of others. Do not share personal information without explicit consent.

** 5. No Tolerance for Malicious Mischief

Disruptive behavior, such as trolling, spamming, or inciting arguments, is prohibited. Focus on contributing positively.

* Enforcing the Guidelines

Our guidelines are designed to ensure that the community remains a safe and welcoming space for all members.

It's important that they are applied consistently and fairly to maintain the trust and respect of the community members.

Here is the process we will follow to handle any violations of the guidelines:

** 1. Report Violations

If you see something that you feel violates the guidelines above you should feel free to send an e-mail to `moderators` at `systemcrafters.net` to let us know about it.

Reports sent via this channel will be kept confidential and will be taken seriously.

On the forum, you can also report posts using the flag icon at the bottom of the post.

** 2. Assessment

When we receive a report, we will look into it.  This may require contacting the parties involved to understand more.  The identity of the reporting member will be kept confidential.

We will first attempt to discuss the issue with the reported party to provide an opportunity to correct the action without further consequences.

** 3. Consequences

Based on the assessment, appropriate consequences may be administered. These may include a warning, temporary suspension, permanent ban, or other actions deemed necessary.

Consequences will be proportionate to the severity of the violation and the member's history.

** 4. Communication of Decision

The decision and any consequent actions will be communicated to both the reporting and reported members.

Details of the decision may be kept private, depending on the nature of the violation.

** 5. Appeals Process

Members have the right to appeal decisions. The appeal must be submitted within 1 week after the decision.

An independent review by a different set of moderators or community leaders may be conducted for appeals.
